package com.example.crime

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fraqment_dialog.*
import kotlinx.android.synthetic.main.fraqment_dialog.view.*

class DialogFragment: DialogFragment() {

    lateinit var activityInterractor : ActivityInterractor

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView: View = inflater.inflate(R.layout.fraqment_dialog, container, false)

        rootView.btn_change_date_and_time.setOnClickListener() {
            DateAndTimeClass.date = editTextDate.text.toString()
            DateAndTimeClass.time = editTextTime.text.toString()

            Toast.makeText(context, "Дата и время успешно изменены", Toast.LENGTH_LONG).show()

            dismiss()
        }

        return rootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ActivityInterractor) {
            this.activityInterractor = context
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activityInterractor.OnFragmentClosed()
    }
}