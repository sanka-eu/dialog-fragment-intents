package com.example.crime

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ActivityInterractor {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        text_crime.text = "Преступление было совершенно группой лиц " + DateAndTimeClass.date +
                " в " + DateAndTimeClass.time + "\nНарушители будут наказаны по всей строгости закона!"
        goto_change_date_and_time.setOnClickListener() {
            var dialog = DialogFragment()
            dialog.show(supportFragmentManager, "customDialog")
        }
    }

    override fun OnFragmentClosed() {
        text_crime.text = "Преступление было совершенно группой лиц " + DateAndTimeClass.date +
                " в " + DateAndTimeClass.time + "\nНарушители будут наказаны по всей строгости закона!"
    }
}